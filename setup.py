from setuptools import setup

setup(
    name='dogetipbot-telegram',
    py_modules=['dogetipbot_telegram'],
    version='1.0',
    description='Telegram bot for tipping Dogecoins',
    author='Paul TREHIOU',
    author_email='paul@nyanlout.re',
    url='https://gitea.nyanlout.re/nyanloutre/dogetipbot-telegram',
    license='MIT',
    entry_points={
        'console_scripts': [
            'dogetipbot-telegram=dogetipbot_telegram:main'
        ]
    },
    install_requires=[
          'python-telegram-bot',
          'requests',
          'pycoin',
          'sqlalchemy'
    ]
)
