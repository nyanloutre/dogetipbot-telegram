{
  description = "A dogecoin Telegram tip bot";

  inputs.nixpkgs.url = "nixpkgs/nixos-21.05";

  outputs = { self, nixpkgs }: let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system}.python3.pkgs;
    packageName = "dogetipbot-telegram";
    app = pkgs.buildPythonApplication rec {
      pname = packageName;

      version = builtins.substring 0 8 self.lastModifiedDate;

      src = ./.;

      doCheck = false;

      propagatedBuildInputs = with pkgs; [
        python-telegram-bot
        requests
        pycoin
        sqlalchemy
      ];
    };
    module = ({ config, lib, pkgs, ... }: let
      cfg = config.${packageName};
    in {
      options.${packageName} = with lib.options; {
        enable = mkEnableOption packageName;
      };

      config = lib.mkIf cfg.enable {
        systemd.services.dogetipbot-telegram = {
          after = [ "network-online.target" ];
          wants = [ "network-online.target" ];
          wantedBy = [ "multi-user.target" ];
          script = "${app}/bin/dogetipbot-telegram --db-path $STATE_DIRECTORY/users.db";
          enable = true;
          serviceConfig = {
            EnvironmentFile = "/mnt/secrets/dogetipbot-telegram_env";
            DynamicUser = true;
            StateDirectory = "dogetipbot";
          };
        };
      };
    });
  in {
    defaultPackage.${system} = app;
    nixosModules.${packageName} = module;
    nixosModule = self.nixosModules.${packageName};
  };
}
